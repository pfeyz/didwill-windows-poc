=========================================
 Didwill windows freeze proof-of-concept
=========================================

This repository exists as a quick proof-of-concept for the following features:

- a flask app
- that uses sqlalchemy and sqlite
- can write to excel files using xlwt
- is frozen as a windows executable

Building the app for windows
============================

.. code::

    $ git clone git@bitbucket.org:pfeyz/didwill-windows-poc.git
    $ cd didwill-windows-poc
    $ python setup.py build

    [lots of output, hopefully no errors]

    $ cd build/win-something?/
    $ ./app.exe

    At this point a webbrowser should open with the didwill experiment running.
