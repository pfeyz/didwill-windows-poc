# setup script adapted from
# https://www.reddit.com/r/Python/comments/21evjn/is_it_possible_to_deploydistribute_flask_as_an/cgcgm9x
# https://stackoverflow.com/a/41881598
# https://stackoverflow.com/a/43034479

import os
import os.path
import sys
import datetime

PYTHON_INSTALL_DIR = os.path.dirname(os.path.dirname(os.__file__))
os.environ['TCL_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tcl8.6')
os.environ['TK_LIBRARY'] = os.path.join(PYTHON_INSTALL_DIR, 'tcl', 'tk8.6')

from cx_Freeze import setup, Executable

# This is ugly. I don't even know why I wrote it this way.
def files_under_dir(dir_name):
    file_list = []
    for root, dirs, files in os.walk(dir_name):
        for name in files:
            file_list.append(os.path.join(root, name))
    return file_list


includefiles = [
            os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tk86t.dll'),
            os.path.join(PYTHON_INSTALL_DIR, 'DLLs', 'tcl86t.dll'),
         ]
for directory in ('static', 'templates', 'data'):
    includefiles.extend(files_under_dir(directory))

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Win32GUI"

dt = datetime.datetime.now()
main_executable = Executable("app.py", base=base)
setup(name="Example",
      version="0.3." + dt.strftime('%m%d.%H%m'),
      description="Example Web Server",
      options={
          'build_exe': {
              'packages': [
                  'jinja2.ext',
                  # 'flask_sqlalchemy',
                  'asyncio',
                  # 'flask_login',
                  # 'flask_wtf',
                  'os',
                  'xlwt',
                  # 'flask_sqlalchemy._compat',
                  'sqlalchemy.dialects.sqlite',
                  'sqlalchemy'
              ],
              'include_files': includefiles,
              'include_msvcr': True}},
      executables=[main_executable], requires=['flask', 'wtforms'])
