function trialListObject(trialLetter){
    this.index = 0;
    this.letter = trialLetter;
    this.trialData = {};
    this.metaData = {};
}

trialListObject.prototype.absorb = function(serverdata){
    this.trialData = serverdata['videoList'];
    this.metaData = serverdata['metaData'];
};

trialListObject.prototype.advance = function(){ this.index++; };

trialListObject.prototype.finished = function(){
    return (this.index == this.trialData.length - 1);
};

trialListObject.prototype.giveData = function(){
    // give the user local data
};

trialListObject.prototype.currentVideo = function(){
    return this.trialData[this.index]["Name"];
};

// turns a regular triaListObject into a practice trial
function makePractice(trialObject){
    trialObject.upload = function(){};
    trialObject.correct = 0;
    trialObject.finished = function(){
    	if(((this.index >= 5) && (this.correct - 1 && this.index) &&
    	   ((this.correct - 1) / this.index) > .80) || this.index >= 7)
    	    return true;
    	else
    	    return false;
    };

    trialObject.recordData = function(dgram, formData){
    	if(dgram.choice == this.trialData[this.index]["correct"]){
    	    this.correct++;
    	    this.lastCorrect = true;
    	}else{
    	    this.lastCorrect = false;
    	}
        dgram['data'] = this.trialData[this.index];
        dgram['metadata'] = formData;
        post('/response', dgram);
        with(this){
    	    report("correct: " + correct);
    	    report("total: " + (index + 1));
    	    if(correct && index + 1)
		report("correct/total: " + (correct/(index + 1 )));
    	}
    };

}

/* accepts a datagram "packet". dgram the values in dgram.choice,
 * dgram.time, and dgram.duration are all stored in the
 * trialData[index] associative array.  if debugForm is specified
 * that data is then uploaded to the server */
trialListObject.prototype.recordData = function(dgram, formData){
    //(dgram.choice, dgram.time, dgram.videoDuration
    this.trialData[this.index]["choice"] = dgram.choice;
    this.trialData[this.index]["reaction time"] = dgram.time;
    this.trialData[this.index]["stimuli length"] = dgram.duration;
    dgram['data'] = this.trialData[this.index];
    dgram['metadata'] = formData
    post('/response', dgram);
    // if(connectedToNetwork)
    //     this.upload();
    // else
    //     this.localStore();
};


// uploads the current trial data iteration to the server
trialListObject.prototype.upload = function(){
    var http_request = new XMLHttpRequest();
    var packagedData = {
	"listIteration": this.trialData[this.index],
	"metaData": this.metaData
    };

    var jsonToSend = JSON.stringify(packagedData);

    // URL HERE -----------------------------------------------------------------------
    //    var url = "http://paulfeitzinger.com/did-will-remix/scripts/setList.cgi?json=" + jsonToSend;
    var url = "scripts/setList.cgi?json=" + jsonToSend;
    // --------------------------------------------------------------------------------
    http_request.open("GET", url, true);
    http_request.send(null);
    http_request = null;
};

trialListObject.prototype.localStore = function(){
    if(localStorage.storedTrialIndex == null){
	localStorage.setItem('counter', 0);
    }
    localStorage.setItem(localStorage.counter, JSON.stringify(this));
};

function videoPlayerObject(frame, _input, _age){
    var container = frame;
    var inputMethod = _input;
    var age = _age;
    var element = document.createElement('video');
    var lastVideo = "";
    element.setAttribute("id", 'vid');
    $(frame).append(element);
    // play instructional video
    this.playInstructions = function(){
	$('#vid').remove();
	$(container).append('<video id="vid"></vid>');
	$.loading(true, {align: 'center'});
	$('#vid').attr('autoplay', 'true');
	$('#vid').attr('autobuffer', 'true');
	$('#vid').attr('src', pickInstrVideo(age, inputMethod));
	$('#vid').hide();
	$('#vid').trigger('pause');
	$('#vid').unbind('ended');
	$('#vid').bind('ended', function(){
			   $('#vid').unbind('ended');
			   $('#vid').unbind('canplaythrough');
			   $().trigger('playbackEnded', $("#vid").attr("duration"));
		       });
	$('#vid').bind('canplaythrough', function(){
			   $.loading(false, {align: 'center'});
			   $('#vid').show();
			   $('#vid').trigger('play');
		       });
	report('loaded instructional video: ' + $('#vid').attr('src'));
    };

    // loads video, displays "get ready!" text, and play video
    // triggers playbackEnded
    this.play = function(videoName){
	report('attemping .play');
	$('#vid').attr('src', videoDirectory + videoName + ".ogg");
	lastVideo = videoName;
	$('#vid').attr('autoplay', 'true');
	$('#vid').attr('autobuffer', 'true');
	$('#vid').trigger('load');
	$('#vid').hide();
	$('#vid').trigger('pause');
	report("source: " + $('#vid').attr('src'));
	report("length: " + $('#vid').attr('duration')*1000);
	if(age == "adult")
	    $.loading(true, {text: "Get Ready!", align: "center"});
	// if(!practicing)
	//     bindPause();
	playQueue.push(true);
	setTimeout(function(){
		       $.loading.fade.time = 100;
		       $().bind('unpauseExperiment', function(){
				    report("running unpauseExperiment");
				    $.loading(false, {img: "/static/img/happy_face.gif", align:"center"});
				    paused = false;
				    if(age == "adult"){
					$.loading.css.fontSize = "300%";
					$.loading(true, {text: "+", align: "center"});
				    }
				    else{
					$("#bell").trigger('play');
					$.loading(true, {img: "/static/img/happy_face.gif", align:"center"});
				    }

				    setTimeout(function(){
						   eraseTheReadyText();
					       }, 2000);
				});
		       setTimeout(function(){
				      if(!paused){
					  report("not paused, triggering unpauseExperiment");
					  $().trigger('unpauseExperiment');
				      }
				      else{
					  report("we're paused, put up the waitForSpaceBar screen.");
					  waitForSpaceBar("Paused: Press Spacebar to Resume",
							  function(){},
							  function(){
							      $().trigger('unpauseExperiment');
							      $().unbind('unpauseExperiment');
							  });
				      }
				  }, 1500);
		   }, (age == "adult") ? 2000 : 0); // only wait if the prtcpt is an adult

    };

    this.playWithFeedback = function(experiment){
	var videoName = experiment.currentVideo();
	var feedback = "";
	$('#vid').attr('src', videoDirectory + videoName + ".ogg");
	$('#vid').attr('autoplay', 'true');
	$('#vid').attr('autobuffer', 'true');
	$('#vid').trigger('load');
	$('#vid').hide();
	$('#vid').trigger('pause');
	report("source: " + $('#vid').attr('src'));
	report("length: " + $('#vid').attr('duration')*1000);
	// display the "get ready!" for 2 seconds
    	if(!experiment.lastCorrect)
	    feedback = "in";
	feedback += "correct";
	if(age == "adult")
	    $.loading(true, {text: feedback, align: "center"});
	setTimeout(function(){
		       $.loading(false, {text: feedback, align: "center"});
		       $.loading.fade.time = 100;
		       $.loading(false, {img: "/static/img/happy_face.gif", align:"center"});
		       setTimeout(function(){
				      if(age == "adult"){
					  $.loading.css.fontSize = "300%";
					  $.loading(true, {text: "+", align: "center"});
				      }
				      else{
					  $("#bell").trigger('play');
					  $.loading(true, {img: "/static/img/happy_face.gif", align:"center"});
				      }
				      setTimeout(function(){
						     eraseTheReadyText();
						     $.loading(false);
						 }, 2000);
				  }, 500);
		   }, 1500);

	// these show the statistics
    	//     $('.ready').append(experiment.correct + ' / ' + experiment.index);
    	//     if(experiment.correct && experiment.index)
    	// 	$('.ready').append("<br>(" +Math.round((experiment.correct/experiment.index) * 100) + "%)");


    };


    // turns on input capture
    // triggers gotUserInputEvent
    this.acceptInput = function(timeThen, length, experiment, inputMethod){
	report('input method: ' + inputMethod);
	if(inputMethod == "keyboard"){
	    $().bind('keypress', function(e){
			 var currentTime = Date.now();
			 if(e.which == 122 || e.which == 109){
			     $().unbind('keypress');
//			     bindModifiers();
			     var LorR = (e.which == 122) ? "L" : "R";
			     $().trigger({type:"gotUserInput", choice:LorR,
					  time:(currentTime - timeThen), duration:length});
			 }});
	}
	else{
	    $(container).bind('click',
			      function (e){
				  var currentTime = Date.now();
				  $(container).unbind('click');
				  var mouseX = e.clientX - container.offsetLeft;
				  var vidWidth = container.clientWidth;
				  var LorR = ((mouseX < vidWidth / 2) ? "L" : "R");
				  $().trigger({type:"gotUserInput", choice:LorR,
					       time:(currentTime - timeThen), duration:length});

			      });
	}
    };

    // turns off input capture
    this.rejectInput = function(){
	$(container).unbind('click');
	report('input capture cancelled');
    };
}
