var debugging = false;
var connectedToNetwork = false;
var remoteVidDir = "http://cunylarc.org/videos/ogg/";
var videoDirectory;

var expStage = 0; // a state variable which allows us to keep track of
// where we are in an experimental run. at
// different stages, the same events being triggered
// are handled in different ways.

var experiment; // a reference which will be used to refer to
// practiceTrial and realTrial at different points in
// the experiment.
var practiceTrial;
var realTrial;
var videoPlayer;
var letter;
var formData = {};
var dataRecieved = {};
var seenInstructions = false;
var inputMethod;
var practicing = false; // necessary for the number-correct display. kinda of kludge-y
var paused = false;
var defferedFun;
var restartedQueue = new Array;
var playQueue = new Array;
var pausedAndReplay = false;

// function bindRestart(){
//     report('binding restart');
//     $().bind("keypress", function(e){
// 		 if(e.which == 82){
// 		     $().unbind('keypress');
// 		     $().trigger('restartVideo');
// 		 }
// 	     });
// }

// function bindPause(){
//     report('binding pause');
//     $().bind("keypress", function(e){
// 		 if(e.which == 80){
// 		     report('pausing');
// 		     paused = true;
// 		     $().unbind('keypress');
// 		 }
// 	     });
// }

// function bindModifiers(){
//     bindPause();
//     bindRestart();
// }

// for firebug console output. if debugging is true, then the function
// report(text) will write TEXT to the firebux console.
window.onload = function(){
    $.loading.css.fontSize = "300%";
    if(typeof(console) != "undefined")
	debugging = true;
    $("#bell").attr('volume', .25);
};

// the first step in the experiment. the experimenter has just clicked
// one of the lettered buttons which fires launch.
$().bind('launch', function(event){
	     report('!launch');
	     letter = event.letter;
	     realTrial = new trialListObject(event.letter);
	     practiceTrial = new trialListObject(event.letter);
	     incrementStage(); // starts the experiment
	 });

$().bind('fetchedData',function(dataRecieved){
	     report('!fetchedData');
	     report(dataRecieved);
	     // for practice rather than
	     // a real trial run.
	     practiceTrial.absorb(dataRecieved.d['practice']);
	     report(dataRecieved.d['practice']);
	     realTrial.absorb(dataRecieved.d['real']);
	     makePractice(practiceTrial); // makes the object suitable
	     $.each(formData, function(key, value){
			realTrial.metaData[key] = value;
		    });

	     if(seenInstructions)
		 incrementStage();
	 });


/* the control flow of the experiment is encoded here. a single
 * initial invocation of incrementStage (once the experimenter has
 * entered the data in the forms) will steer the experiment to completion. */
function incrementStage(){
    expStage++;
    switch(expStage){

    case 1:
	report('ENTERING STAGE 1 : Printed Instructions');

	collectFormData(formData);
        formData.server = 'local';

	if(formData.server == "local"){
	    videoDirectory = "./static/videos/ogg/" + formData.vidsize + "/";
	    connectedToNetwork = false;
	}

	if(formData.server == "remote")
	{
	    videoDirectory = remoteVidDir + formData.vidsize + "/";
	    connectedToNetwork = true;
	}

	if(!(formData.age && formData.inputMethod)){
	    throw new Error("form data was not properly collected");
	}
	if(formData['age'] == 'child'){
	    report("particpant is a child. skipping textual instructions");
	    waitForSpaceBar("Press Spacebar to Start",
			    function(){ $('#trialForm').remove(); } ,
			    function(){ incrementStage(); } );
	    break;
	}

	/* control is temporarily passed to this function. it is
	 * responsible for calling incrementStage() (!)
	 * clears and writes to trialForm */
	textInstructions(formData["inputMethod"]);
	break;

    case 2:	 // video instructions
	report('ENTERING STAGE 2 : Video Instructions');
	$('#trialForm').remove();
	videoPlayer = new videoPlayerObject(document.getElementById('vidContainer'),
					    formData['inputMethod'], formData['age']);
	videoPlayer.playInstructions(formData['inputMethod']);
	fetchTrialData(letter, connectedToNetwork); // puts data in fetchedTrialData

	$().bind('playbackEnded', function(){
		     report('!playbackEnded');
		     seenInstructions = true;
		     if(dataRecieved){
			 waitForSpaceBar("Press Spacebar to Continue",
					 function(){ $("#vid").hide(); },
					 function(){ incrementStage(); });
		     }
		 });
	break;


    case 3:	 // running practice trial
	report('ENTERING STAGE 3 : Practice Trial');
	$('#vid').unbind('ended');
	practicing = true;
	experiment = practiceTrial;
	videoPlayer.play(experiment.currentVideo());

	$().unbind('playbackEnded');
	$().bind('playbackEnded', function(data){
		     report('!playbackEnded');
		     report(data);
		     videoPlayer.acceptInput(data.timeThen, data.duration, experiment, formData["inputMethod"]);
		 });
	var bindUserInput = function(){
	    $().bind('gotUserInput', function(data){
			 report('!gotUserInput');
			 report(data.choice);
			 report("reaction time :" + data.time);
                         experiment.recordData(data, formData);
			 if(experiment.finished()){
			     incrementStage();
			 }
			 else{
			     experiment.advance();
			     if(expStage == 3)
				 videoPlayer.playWithFeedback(experiment);
			     else
				 videoPlayer.play(experiment.currentVideo());
			 }
		     });};
	bindUserInput();
	$().bind('pauseSession', function(){
		     report('!pauseSession');
		     $().unbind('gotUserInput');
		     $().bind('gotUserInput', function(data){
				  $().unbind('gotUserInput');
				  report('!gotUserInput-pauseSession');
				  report(data.choice);
				  report("reaction time :" + data.time);
			          experiment.recordData(data, formData);
				  $('#vid').hide();
				  $.loading({text: "Paused. Press SPACEBAR to resume experiment", align: "center", pulse: ""});
				  $().bind('keypress', function(e){
					       if(e.which == 32){
						   $.loading(false);
						   $().unbind('keypress');
//						   bindModifiers();
						   bindUserInput();
						   if(experiment.finished()){
						       incrementStage();
						   }
						   else{
						       experiment.advance();
						       videoPlayer.play(experiment.currentVideo());
						   }
					       }});
			      });
		 });

	$().bind('restartVideo', function(){
		     videoPlayer.rejectInput();
		     if(playQueue.length > 0){
			 restartedQueue.push(true);
			 report('pusing onto restart queue');
		     }
		     report('!restartVideo');
		     $().unbind('gotUserInput');
		     $("#vid").trigger('pause');
		     $("#vid").hide(0);
		     waitForSpaceBar("Press Spacebar to Replay Trial",
				     function(){},
				     function(e){
					 bindUserInput();
					 paused = false;
					 if(experiment.finished()){
					     incrementStage();
					 }
					 else{
					     videoPlayer.play(experiment.currentVideo());
					 }
				     });
		 });
	break;

    case 4: // take a break
	report('ENTERING STAGE 4 : Rest Between Trials');
	$().unbind('playbackEnded');
	$().unbind('gotUserInput');
	$('#vid').hide();
	$('#vidContainer').append('<div class="ready">Press SPACEBAR to start the real trials</div>');
	$().bind('keypress', function(e){
		     if(e.which == 32){
			 $().unbind('keypress');
			 $('.ready').remove();
			 incrementStage();
		     }});
	break;

    case 5:  // running actual trial
	report('ENTERING STAGE 5 : Actual Trial');
	practicing = false;
	experiment = realTrial;
	videoPlayer.play(experiment.currentVideo());
	$().bind('playbackEnded', function(data){
		     report('!playbackEnded');
		     videoPlayer.acceptInput(data.timeThen, data.vidLength, experiment, formData["inputMethod"]);
		 });
	bindUserInput = function(){
	    $().bind('gotUserInput', function(data){
			 report('!gotUserInput');
			 report(data);
			 report("reaction time: " + data.time);
		         experiment.recordData(data, formData);
			 if(experiment.finished()){
			     incrementStage();
			 }
			 else{
			     experiment.advance();
			     videoPlayer.play(experiment.currentVideo());
			 }
		     });
	};
	bindUserInput();
	break;


    case 6:	 // ending entire experiment
	report('ENTERING STAGE 6 : Ending');
	xlify();
	if(!connectedToNetwork)
	    localStorage.counter++;
	$().unbind('playbackEnded');
	$().unbind('gotUserInput');
	$('#vid').hide();
	$('#vidContainer').append('<div class="ready">Thanks for Participating in the Experiment!</div><br><br><br>');
	$('#vidContainer').append('<button id="jsonGetter">click here to download the experimental data recorded</button>');
	$('#jsonGetter').bind('click', function(){
				  var name = realTrial.metaData["subject name"] + "-" + realTrial.metaData["subjectId"];
				  alert('Now, after clicking OK, go to File>Save As and save this file as ' + name + '.html');
				  document.write(JSON.stringify(realTrial));
			      });
	break;
    }
}
