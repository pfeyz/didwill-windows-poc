#!/home/clarc/pyenv/bin/python

import cgi
import jsonlib as json
import csv
import pickle

query = cgi.FieldStorage()
listRequest = query.getvalue("json")
data = json.loads(listRequest)
idNum = data["metaData"]["subjectId"]
trialData = dict()

print "Content-type: application/json\n\n"

try:
    trialFile = open("../temp-data/" + str(idNum) + ".pk", 'r')
    trialData = pickle.load(trialFile)
    trialFile.close()

except IOError:
    trialData["dataList"] = list()

trialData["dataList"].append(data["listIteration"])
trialData["metaData"] = data["metaData"];

trialFile = open("../temp-data/" + str(idNum) + ".pk", 'w')

pickle.dump(trialData, trialFile)

trialFile.close()
