#!/home/clarc/pyenv/bin/python
# SERVER ^
#!/usr/bin/python
# LOCAL ^

"""
getList sends didWill json representing data for a trial run based on
the trial letter didWill specifies in the calling url.

this local version is for development, and is not actually a cgi script
"""

import jsonlib as json
import csv
import pickle
import cgi

from sys import argv

print "Content-type: application/json\n\n"

def getAndIncrementSeqId():
    """ Retrieves the current subject ID from disk, and increments the counter file """
    try:
        counterFile = open("idCounter.pk", 'r')
        idNum = pickle.load(counterFile)
        counterFile.close()
    except IOError:
        idNum = 0
    counterFile = open("idCounter.pk", 'w')
    pickle.dump(idNum + 1, counterFile)
    counterFile.close()
    return idNum

def getJsonFromMasterList(letterOfList):
    """ Retrieves """
    columnHeaders = list()  #  will hold the same keys for every
                            #  dictionary entry in videoList

    trialList = list()  #  will hold values for dictionary component
                        #  of videoList

    videoList = list()  #  will hold dictionaries created from
                        #  columnHeaders:trialList[index] pairs
    practiceList = list()

    metaData = dict()

    trialData = dict()  #  dictionary that serves as wrapper around
                        #  all data; recieved by client in
                        #  serverFetch(trial) [dwoTools.js]
    practiceData = dict()
    letterOfList = letterOfList.lower()
    if letterOfList in ['a','b','c','d']:
        filename = "../trialLists/masterList.csv"

        csvFile = csv.reader(open(filename))
        trialList = list(csvFile)  #  a list containing our videoList data
        columnHeaders = trialList.pop(0) #  extract the headers

        for line in trialList:
            lineInfo = dict()  #  temporary dictionary to be inserted into
            for column, index in zip(line, range(len(line))):
                lineInfo[columnHeaders[index]] = line[index]
            if(lineInfo["List"].lower() == letterOfList): videoList.append(lineInfo)
        metaData["listLetter"] = letterOfList.upper()
        metaData["subjectId"] = getAndIncrementSeqId()

        for line in trialList:
            lineInfo = dict()  #  temporary dictionary to be inserted into
            for column, index in zip(line, range(len(line))):
                lineInfo[columnHeaders[index]] = line[index]
            if((letterOfList in lineInfo["List"].lower()) and
            (lineInfo["statement"] == "practice")):
                practiceList.append(lineInfo)


        trialData["metaData"] = metaData
        practiceData["metaData"] = metaData
        trialData["videoList"] = videoList
        practiceData["videoList"] = practiceList

#        return json.dumps({"practice":trialData, "real":trialData})
        return json.dumps({"practice":practiceData, "real":trialData})

    else:
        return json.dumps(["error", str("'" + letterOfList +
                                        "' is not a valid list identifier")])

if __name__ == "__main__":
    query = cgi.FieldStorage()
    listRequest = query.getvalue("list").lower()
#    listRequest = 'a'
    print getJsonFromMasterList(listRequest)
