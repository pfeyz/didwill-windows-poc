import json
import webbrowser

from models import TrialData, Session

import pandas as pd
from flask import Flask, request, render_template
from waitress import serve

app = Flask(__name__)


@app.route('/')
def serve_results():
    return render_template('index.html')


@app.route('/response', methods=['POST'])
def post_results():
    data = request.get_json()
    session = Session()
    trial = TrialData(data=json.dumps(data))
    session.add(trial)
    session.commit()
    dump_data(session)
    session.close()
    return 'ok'


def make_row(example):
    data = {'timestamp': example['timeStamp']}
    data.update(example['data'])
    data.update(example['metadata'])
    return data


def dump_data(session):
    results = session.query(TrialData).all()
    results = [json.loads(item.data)
               for item in results]
    df = pd.DataFrame([make_row(item) for item in results])
    df.to_excel('data-export.xls')


webbrowser.open('http://0.0.0.0:8080')
serve(app, listen='*:8080')
