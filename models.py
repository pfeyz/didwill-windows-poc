from sqlalchemy import (Column, Integer, String, DateTime, ForeignKey, Enum,
                        Boolean)
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship


engine = create_engine('sqlite:///test.db', echo=False)
Base = declarative_base()


class TrialData(Base):
    __tablename__ = 'trialdata'

    id = Column(Integer, primary_key=True)
    data = Column(String, nullable=False)


# Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
